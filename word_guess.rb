require 'pp'
require 'optparse'

dict_file = '/usr/share/dict/american-english'

options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: word_parse.rb [options]'
  opts.on('-d', '--dict-file', 'Specify path to dictionary file to use') { |f| options[:dict_file] = f }
  opts.on('-a', '--any', 'Don\'t assume that all characters are needed') { |_f| options[:any] = true }
end.parse!

dict = File.readlines(options[:dict_file] || dict_file).map do |word|
#  word.chomp.downcase
  begin 
    word.chomp.downcase.gsub(/[^a-z0-9]/, '')
  rescue ArgumentError; end
end.uniq

dict_hash = {}
thread = Thread.new do 
  dict_hash = dict.group_by do |word|
    next if word.nil?
  	begin 
      #word.chomp.downcase.gsub(/[^a-z0-9]/, '').split(//).sort.join
      word.split(//).sort.join
  	rescue ArgumentError; end
  end
end

input = ARGV.first

abort('You must specify an input string') if input.nil?

puts "Searching a total of #{dict.size} words for '#{input}'..."
puts

groups = input.scan(/(\[\S+?\]|\S)/).flatten

groups.map! do |group|
  group.scan(/\S/).delete_if { |letter| %w([ ]).include?(letter) }
end

input_combos = []
min = options[:any] ? 1 : groups.size
groups.size.downto(min) do |group_size|

  puts "Running for all input combinations of size #{group_size}..."

  all_combos = groups.combination(group_size).to_a

  all_combos.each do |combo|
    combinations = nil
    combo.each_with_index do |group, index|
      break if index + 1 == combo.size
      combinations ||= group
      combinations = combinations.product(combo[index + 1])
    end

	input_combos << combinations unless combinations.nil?
  end
end

puts
puts 'Waiting for dictionary parse to finish...'
thread.join
puts "Dict parse finished, got #{dict_hash.keys.size} total keys."
puts

possible_words = []
input_combos.each do |combinations|
  combinations.each do |combo|
    word = combo.flatten.join.downcase.gsub(/[^a-z0-9]/, '').split(//).sort.join
    dict_words = dict_hash[word]
	next if dict_words.nil?
    puts "Found #{dict_words.uniq.size} total words for: '#{word}'..."
	pp #{dict_words}
    possible_words << dict_words.uniq
  end
end

possible_words = possible_words.flatten.uniq.sort_by(&:length).reverse

#possible_words.keep_if { |word| word =~ /^[mwe]/ }
#possible_words.keep_if { |word| word =~ /^[mwe][wemohis]/ }

puts
File.open('word_parse_output.txt', 'w') do |file|
  possible_words.each do |word|
    file.write(word + "\n")
  end
end
